package com.example.eca.Fragments;

import com.example.eca.Notifications.MyResponse;
import com.example.eca.Notifications.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAwmGPVfI:APA91bGSQoXTZQBnBuugKOk4Box8ddTesSFMNkmrnsID5h0MwwJythI9Z9_ZE4Pz9doIqB11L6eDmlpafuH-pqTzZzs5DwAn-Tl3FQYGQQuOXDn34Ql5ySh-Aa2sC6JJtTJX6doiMK5r"
            }
    )
    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}
