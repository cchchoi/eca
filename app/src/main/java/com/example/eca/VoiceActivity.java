package com.example.eca;

import android.app.ProgressDialog;
import android.media.MediaRecorder;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;

public class VoiceActivity extends AppCompatActivity {

    private Button btn_record;
    private TextView txt_record;

    private MediaRecorder recorder;

    private String fileName;

    private static final String LOG_TAG = "AudioRecordTest";

    private StorageReference mStorage;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice);
        btn_record = findViewById(R.id.btn_record);
        txt_record = findViewById(R.id.txt_record);

        mStorage = FirebaseStorage.getInstance().getReference();
        progressDialog = new ProgressDialog(this);

        fileName = getExternalCacheDir().getAbsolutePath();
        fileName += "/audiorecordtest2.3gp";

        btn_record.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN){

                    startRecording();
                    txt_record.setText("Recording");

                }else if (motionEvent.getAction() == MotionEvent.ACTION_UP){

                    stopRecording();
                    txt_record.setText("Stopped");

                }
                return false;
            }
        });

    }

    private void startRecording() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setOutputFile(fileName);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        recorder.start();
    }

    private void stopRecording() {
        recorder.stop();
        recorder.release();
        recorder = null;

        uploadAudio();
    }

    private void uploadAudio() {
        StorageReference filepath = mStorage.child("Audio");

        progressDialog.setMessage("Uploading...");
        progressDialog.show();

        Uri uri = Uri.fromFile(new File(fileName));

        filepath.child("1").putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                progressDialog.dismiss();
                txt_record.setText("finish");
            }
        });
    }
}
